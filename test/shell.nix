{
  pkgs ? import <nixpkgs> { },
}:

let
  h5-artiq = import ../default.nix { inherit pkgs; };
in
pkgs.mkShell {
  buildInputs = [
    (pkgs.python3.withPackages (
      ps:
      [
        # Packages required for testing
        ps.mypy
        ps.flake8
      ]
      ++ h5-artiq.propagatedBuildInputs
    ))
  ];
}
