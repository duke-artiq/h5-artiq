# H5 ARTIQ

This repository contains utilities for [ARTIQ](https://github.com/m-labs/ARTIQ) HDF5 archive files.

## Installation

The h5-artiq package requires Python 3.7 or newer.

### Nix Flakes

When using Nix Flakes, a shell with the H5 ARTIQ packages can be directly opened from the terminal.

```shell
nix shell gitlab:duke-artiq/h5-artiq
```

The repository can also be added as an input in your own Nix Flake.

```nix
{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-22.11";
    h5-artiq = {
      url = "gitlab:duke-artiq/h5-artiq";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = {
    # Package available as h5-artiq.packages.${system}.h5-artiq
  };
}
```

### Nix

It is possible to build the package directly from source in your Nix shell if you have access to
the [M-labs Nix channel](https://www.m-labs.hk/artiq/manual/installing.html).

```nix
let
  pkgs = import <nixpkgs> { };
  artiqpkgs = import <artiq-full> { inherit pkgs; }; # We assume the M-labs Nix channel is already configured
  h5-artiq = pkgs.callPackage (import (builtins.fetchGit "https://gitlab.com/duke-artiq/h5-artiq.git")) { inherit pkgs artiqpkgs; };
in
pkgs.mkShell {
  buildInputs = [
    (pkgs.python3.withPackages (ps: [
      h5-artiq
    ]))
  ];
}
```

It is also possible to build the h5-artiq Nix package using the Nix scripts in the repository.

```shell
git clone https://gitlab.com/duke-artiq/h5-artiq.git
cd h5-artiq/
nix-shell
```

### Poetry

Users can add h5-artiq as a git dependency to their package.

```toml
[tool.poetry.dependencies]
h5-artiq = { git = "https://gitlab.com/duke-artiq/h5-artiq.git" }
```

With poetry, it is possible to build the package locally using a poetry shell.

```shell
git clone https://gitlab.com/duke-artiq/h5-artiq.git
cd h5-artiq/
poetry shell
poetry install
```

### Pip

Pip can also be used to install h5-artiq if sipyco is installed manually first.

```shell
pip install git+https://github.com/m-labs/sipyco
pip install git+https://gitlab.com/duke-artiq/h5-artiq
```

### Conda

For Conda users, h5-artiq can be installed using pip, but dependencies have to be installed manually using Conda and
the [M-labs Conda channel](https://www.m-labs.hk/artiq/manual/installing.html).
See [this example Conda environment file](environment.yml).

## Usage

The h5-artiq package can be imported as a library to use in your own programs.

```python
from h5_artiq.reader import H5Reader
from h5_artiq.helpers import pretty_print

reader = H5Reader('~/artiq_archive.h5')
pretty_print(reader.get_datasets())
```

The h5-artiq package also contains a CLI that can be used to print contents of an ARTIQ HDF5 archive file.
See the help message for more information about the options.

```shell
h5_artiq_print datasets ~/artiq_archive.h5
```

## Testing

The code in this repository can be tested by executing the following commands in the root directory of the project.

```shell
mypy    # Requires mypy to be installed
flake8  # Requires flake8 to be installed
```
