{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-22.11";
    flake-utils.url = "github:numtide/flake-utils";
    sipyco = {
      url = "github:m-labs/sipyco";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs =
    {
      self,
      nixpkgs,
      flake-utils,
      sipyco,
    }:
    flake-utils.lib.eachDefaultSystem (
      system:
      let
        pkgs = nixpkgs.legacyPackages.${system};

        # h5-artiq package
        version = "${builtins.toString self.sourceInfo.lastModifiedDate or 0}+${
          self.sourceInfo.shortRev or "unknown"
        }";
        h5-artiq = pkgs.python3Packages.callPackage ./derivation.nix {
          inherit (sipyco.packages.${system}) sipyco;
          inherit version;
        };

        # ci versions of packages because docker makes sipyco tests fail
        sipyco-ci = sipyco.packages.${system}.sipyco.overridePythonAttrs (oa: {
          doCheck = false;
        });
        h5-artiq-ci = pkgs.python3Packages.callPackage ./derivation.nix {
          sipyco = sipyco-ci;
          inherit version;
        };

      in
      {
        packages = {
          inherit h5-artiq sipyco-ci h5-artiq-ci;
          default = h5-artiq;
        };

        devShells = {
          default = pkgs.mkShell {
            buildInputs = [
              (pkgs.python3.withPackages (
                ps:
                [
                  ps.mypy
                  ps.flake8
                ]
                ++ h5-artiq.propagatedBuildInputs
              ))
            ];
          };

          ci = pkgs.mkShell {
            buildInputs = [
              (pkgs.python3.withPackages (
                ps:
                [
                  ps.mypy
                  ps.flake8
                ]
                ++ h5-artiq-ci.propagatedBuildInputs
              ))
            ];
          };
        };

        formatter = pkgs.nixfmt-rfc-style;
      }
    );
}
