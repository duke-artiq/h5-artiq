{
  pkgs ? import <nixpkgs> { },
  artiqpkgs ? import <artiq-full> { inherit pkgs; },
}:

pkgs.python3Packages.callPackage ./derivation.nix { inherit (artiqpkgs) sipyco; }
