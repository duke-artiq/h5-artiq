{
  pkgs ? import <nixpkgs> { },
}:
let
  h5-artiq = import ./default.nix { inherit pkgs; };
in
(pkgs.python3.withPackages (ps: [ h5-artiq ])).env
