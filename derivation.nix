{
  buildPythonPackage,
  nix-gitignore,
  lib,
  h5py,
  natsort,
  sipyco,
  version ? "0.1.0",
}:

buildPythonPackage {
  pname = "h5-artiq";
  inherit version;
  src = nix-gitignore.gitignoreSource [
    "*.nix"
    "/*.lock"
  ] ./.;

  propagatedBuildInputs = [
    h5py
    natsort
    sipyco
  ];

  condaDependencies = [
    "python>=3.7"
    "h5py"
    "natsort"
    "sipyco"
  ];

  # No tests
  doCheck = false;

  pythonImportsCheck = [ "h5_artiq" ];

  meta = with lib; {
    description = "Utilities for ARTIQ HDF5 archive files";
    maintainers = [ "Duke University" ];
    homepage = "https://gitlab.com/duke-artiq/h5-artiq";
    license = licenses.mit;
  };
}
