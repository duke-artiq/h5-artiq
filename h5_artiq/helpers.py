import typing
import pprint

import natsort
import h5py

__all__ = [
    'VAL_T', 'SEQ_T', 'GROUP_T', 'OBJ_T',
    'get_value', 'is_sequence', 'convert',
    'pretty_print',
]

VAL_T = typing.Any
"""Type of a value extracted from a dataset."""
SEQ_T = typing.Sequence[typing.Any]  # Should have been a recursive type
"""Type of a dataset sequence."""
GROUP_T = typing.Dict[str, typing.Union[VAL_T, SEQ_T, typing.Any]]  # Should have been a recursive type
"""Type of a dataset group."""
OBJ_T = typing.Union[VAL_T, SEQ_T, GROUP_T]
"""Type of an object, which is ether a value, sequence, or group."""


class _PrettyReprMeta(type):
    """Metaclass to have a pretty representation of a class."""
    _REPR: typing.ClassVar[str]

    def __repr__(cls) -> str:
        return cls._REPR


class _CollapsedList(metaclass=_PrettyReprMeta):
    """Class used to represent a collapsed list."""
    _REPR = '[...]'


def get_value(dataset: h5py.Dataset) -> VAL_T:
    """Get the value inside a dataset.

    This function automatically attempts to decode any values of type :const:`bytes`.

    :param dataset: The dataset
    :return: The value inside the dataset
    """
    assert isinstance(dataset, h5py.Dataset)

    # Extract the value from the dataset
    value = dataset[()]

    if isinstance(value, bytes):
        try:
            # Attempt string decoding
            return dataset.asstr()[()]
        except TypeError:
            pass

    # Return value
    return value


def is_sequence(group: h5py.Group) -> bool:
    """Determine if a group represents a sequence.

    :param group: The group
    :return: :const:`True` if the group represents a sequence
    """
    assert isinstance(group, h5py.Group)

    try:
        # Try to convert all keys to integers
        indices = [int(k) for k in group.keys()]
    except ValueError:
        # Not all keys are integers, so the group is not a sequence
        return False
    else:
        # The group is a sequence if all integer keys are indexes starting at 0
        return all(i == n for i, n in zip(sorted(indices), range(len(indices))))


def convert(obj: typing.Union[h5py.Dataset, h5py.Group], *, sequences: bool = True) -> OBJ_T:
    """Convert a H5 object to a Python representation.

    :param obj: The H5 object
    :param sequences: True if sequences should be automatically detected and converted
    :return: The Python representation of the H5 object
    """

    if isinstance(obj, h5py.Dataset):
        return get_value(obj)
    elif isinstance(obj, h5py.Group):
        if sequences and is_sequence(obj):
            return [convert(obj[k], sequences=sequences) for k in natsort.natsorted(obj.keys())]
        else:
            return {k: convert(v, sequences=sequences) for k, v in obj.items()}
    else:
        raise TypeError


def _collapse_sequences(obj: OBJ_T) -> OBJ_T:
    """Collapse sequences, used for pretty printing."""
    if isinstance(obj, dict):
        return {k: _collapse_sequences(v) for k, v in obj.items()}
    elif isinstance(obj, list):
        return _CollapsedList
    else:
        return obj


def pretty_print(obj: typing.Any, *,
                 collapse: bool = False, **kwargs: typing.Any) -> None:
    """Pretty print the Python representation of an H5 object.

    :param obj: The Python representation of
    :param collapse: Collapse sequences for more compact printing
    :param kwargs: Keyword arguments forwarded to :func:`pprint.pprint`
    """
    pprint.pprint(_collapse_sequences(obj) if collapse else obj, **kwargs)
