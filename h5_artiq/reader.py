import pathlib
import typing

import h5py
import sipyco.pyon

from h5_artiq.helpers import convert, get_value, OBJ_T

__all__ = ['H5Reader']


class H5Reader:
    """Reader for ARTIQ HDF5 archive files."""

    _DATASETS_KEY: typing.ClassVar[str] = 'datasets'
    _ARCHIVE_KEY: typing.ClassVar[str] = 'archive'
    _EXPID_KEY: typing.ClassVar[str] = 'expid'

    _source: h5py.File

    def __init__(self, file: typing.Union[str, pathlib.Path, h5py.File], **kwargs: typing.Any):
        """Initialize the reader.

        :param file: The path to the H5 file or and H5 file object
        :param kwargs: Keyword arguments passed to the :class:`h5py.File` constructor if no H5 file object was passed
        """
        if isinstance(file, h5py.File):
            self._source = file
        else:
            self._source = h5py.File(pathlib.Path(file).expanduser().resolve(), mode='r', **kwargs)

    def get_datasets(self, **kwargs: typing.Any) -> OBJ_T:
        """Get the ARTIQ datasets from the H5 file.

        :param kwargs: Keyword arguments passed to :func:`convert`
        :return: The ARTIQ datasets in a Python representation
        """
        return convert(self._source[self._DATASETS_KEY], **kwargs)

    def get_archive(self, **kwargs: typing.Any) -> OBJ_T:
        """Get the ARTIQ archive from the H5 file.

        :param kwargs: Keyword arguments passed to :func:`convert`
        :return: The ARTIQ archive in a Python representation
        """
        return convert(self._source[self._ARCHIVE_KEY], **kwargs)

    def get_metadata(self) -> typing.Dict[str, typing.Any]:
        """Get the ARTIQ metadata from the H5 file.

        :return: The ARTIQ metadata as a dictionary
        """
        metadata = {k: get_value(v) for k, v in self._source.items() if isinstance(v, h5py.Dataset)}
        if self._EXPID_KEY in metadata:
            metadata[self._EXPID_KEY] = sipyco.pyon.decode(metadata[self._EXPID_KEY])
        return metadata
