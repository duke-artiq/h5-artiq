"""
This module can be run directly to start the MonInj dummy service.

The MonInj dummy service can be used during simulation to reduce the number of error messages observed
in the ARTIQ dashboard. When using DAX.sim, the MonInj dummy service is started automatically.

Note that the MonInj dummy service does not implement the SiPyCo interface and is therefore
not implemented as an ARTIQ controller.
"""

import argparse
import pathlib
import typing

from h5_artiq.reader import H5Reader
from h5_artiq.helpers import pretty_print

__all__ = ['print_h5']


def print_h5() -> None:
    # Define dataset groups
    dataset_groups: typing.Dict[str, typing.Callable[..., typing.Any]] = {
        'datasets': H5Reader.get_datasets,
        'archive': H5Reader.get_archive,
        'metadata': H5Reader.get_metadata,
    }

    # Add arguments
    parser = argparse.ArgumentParser(description='h5-artiq print utility')
    parser.add_argument('group', choices=list(dataset_groups),
                        help='the dataset group to print')
    parser.add_argument('path', type=pathlib.Path,
                        help='path to the HDF5 file')
    parser.add_argument('--collapse', action='store_true',
                        help='collapse sequences for more compact printing')

    pprint = parser.add_argument_group('pprint options')
    pprint.add_argument('--indent', type=int,
                        help='the amount of indentation added for each nesting level')
    pprint.add_argument('--width', type=int,
                        help='the desired maximum number of characters per line in the output')
    pprint.add_argument('--depth', type=int,
                        help='the number of nesting levels which may be printed')
    pprint.add_argument('--compact', action='store_true',
                        help='collapse sequences for more compact printing')

    # Parse arguments
    args = parser.parse_args()

    # Create reader
    reader = H5Reader(args.path)

    # Get group and print
    obj = dataset_groups[args.group](reader)
    pretty_print(obj, **{k: v for k, v in args.__dict__.items() if k not in {'path', 'group'} and v is not None})
